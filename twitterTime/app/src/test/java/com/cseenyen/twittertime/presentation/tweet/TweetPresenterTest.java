package com.cseenyen.twittertime.presentation.tweet;

import com.cseenyen.twittertime.R;
import com.cseenyen.twittertime.domain.interactor.SaveSessionUseCase;
import com.cseenyen.twittertime.domain.interactor.SaveUpdatedTimeUseCase;
import com.cseenyen.twittertime.domain.interactor.SearchUseCase;
import com.twitter.sdk.android.core.AppSession;
import com.twitter.sdk.android.core.models.Tweet;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

/**
 * Created by cseenyen on 07/03/16.
 */
public class TweetPresenterTest {

    private TweetPresenter tweetPresenter;

    @Mock
    TweetLoadDataView tweetLoadDataView;
    @Mock
    SearchUseCase searchUseCase;
    @Mock
    SaveSessionUseCase saveSessionUseCase;
    @Mock
    SaveUpdatedTimeUseCase saveUpdatedTimeUseCase;
    @Mock
    Tweet tweet;
    @Mock
    AppSession appSession;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        tweetPresenter = new TweetPresenter(searchUseCase, saveSessionUseCase, saveUpdatedTimeUseCase);
        tweetPresenter.setView(tweetLoadDataView);
    }

    @Test
    public void onDisplayError_viewDisplayError() {
        tweetPresenter.displayError();

        verify(tweetLoadDataView).hideLoading();
        verify(tweetLoadDataView).displayError(R.string.error_getting_tweet);
    }

    @Test
    public void onTweetReceived_displayTweet() {
        tweetPresenter.displayTweet(tweet);

        verify(tweetLoadDataView).hideLoading();
        verify(tweetLoadDataView).displayTweet(tweet);
    }

}
