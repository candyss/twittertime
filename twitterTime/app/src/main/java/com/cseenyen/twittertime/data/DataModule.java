package com.cseenyen.twittertime.data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

/**
 * Created by cseenyen on 06/03/16.
 */

@Module
public class DataModule {

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient() {
        return new OkHttpClient();
    }

    @Provides
    @Singleton
    @Named("twitterDateFormat")
    public DateFormat provideDateFormat() {
        return new SimpleDateFormat("EEE MMM dd HH:mm:ss", Locale.ENGLISH);
    }

    @Provides
    @Singleton
    @Named("searchDateFormat12")
    public DateFormat provideSearchDateFormat12() {
        return new SimpleDateFormat("HH:mm", Locale.ENGLISH);
    }

    @Provides
    @Singleton
    @Named("searchDateFormat24")
    public DateFormat provideSearchDateFormat24() {
        return new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
    }

}
