package com.cseenyen.twittertime.presentation.splash.di;

import com.cseenyen.twittertime.domain.interactor.SaveSessionUseCase;
import com.cseenyen.twittertime.domain.interactor.UseCase;
import com.cseenyen.twittertime.presentation.internal.di.ActivityScope;
import com.twitter.sdk.android.core.AppSession;

import dagger.Module;
import dagger.Provides;

/**
 * Created by cseenyen on 27/02/16.
 */

@Module
public class SplashModule {

    private AppSession session;

    @Provides
    @ActivityScope
    //@Named("twitterSession")
    UseCase provideSaveSessionUseCase(SaveSessionUseCase saveSessionUseCase) {
        return saveSessionUseCase;
    }

    /*public void setAppSession(AppSession session) {
        this.session = session;
    } */

    /* AppSession provideAppSession() {
        TwitterCore.getInstance().logInGuest(new Callback<AppSession>() {
            @Override
            public void success(Result<AppSession> result) {
                //Log.d(Statics.TAG, "loginGuest.callback.success called");
                //SplashPresenter.this.saveGuestSession(result.data);
                session = result.data;
                //twitterApiClient = TwitterCore.getInstance().getApiClient(guestAppSession);
                Log.e("Repository", "result: " + result.data.getAuthToken());
            }

            @Override
            public void failure(TwitterException exception) {
                //Log.d(Statics.TAG, "loginGuest.callback.failure called");
                // unable to get an AppSession with guest auth
                throw exception;
            }
        });
        return session;
    } */


    /* UseCase provideTwitterGuestSessionUseCase(TweetRepository tweetRepository,
                                              ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new TwitterGuestSessionUseCase(this.session, tweetRepository, threadExecutor, postExecutionThread);
    } */
}
