package com.cseenyen.twittertime.data.repository;

import android.content.SharedPreferences;
import android.util.Log;

import com.cseenyen.twittertime.data.Constants;
import com.cseenyen.twittertime.data.network.TwitterCustomService;
import com.cseenyen.twittertime.data.network.TwitterTimeApiClient;
import com.google.gson.Gson;
import com.twitter.sdk.android.core.AppSession;
import com.twitter.sdk.android.core.models.Search;
import com.twitter.sdk.android.core.models.Tweet;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;

/**
 * Created by cseenyen on 27/02/16.
 */
@Singleton
public class TweetRepositoryImpl implements TweetRepository {

    private AppSession guestAppSession;
    private volatile Boolean twitterFinished;

    @Inject
    SharedPreferences sharedPrefs;
    @Inject
    @Named("twitterDateFormat")
    DateFormat twitterDateFormat;
    @Inject
    @Named("searchDateFormat12")
    DateFormat searchDateFormat12;
    @Inject
    @Named("searchDateFormat24")
    DateFormat searchDateFormat24;

    Gson gson;
    TwitterCustomService customService;

    @Inject
    public TweetRepositoryImpl(Gson gson) {
        this.gson = gson;
    }

    @Override
    public Observable<Boolean> saveLaunchTime() {
        final long launchTime = new Date().getTime();
        return saveTime(launchTime);
    }

    @Override
    public Observable<Boolean> saveSession(final AppSession session) {
        return Observable.create(
                new Observable.OnSubscribe<Boolean>() {
                    @Override
                    public void call(Subscriber<? super Boolean> sub) {
                        String sessionAsString = new Gson().toJson(session);
                        boolean success = sharedPrefs.edit()
                                .putString(Constants.SharedPreferencesConstants.KEY_SESSION, sessionAsString)
                                .commit();
                        sub.onNext(success);
                        sub.onCompleted();
                    }
                }
        );
    }

    @Override
    public Observable<Tweet> timeTweet() {
        return getTweetsWithTime();
    }

    @Override
    public Observable<Boolean> saveUpdatedTime() {
        long minute = 60000l; //  1 minute
        long currentTime = sharedPrefs.getLong(Constants.SharedPreferencesConstants.KEY_LAUNCH_TIME, 0);
        return saveTime(currentTime + minute);
    }

    private Observable<Boolean> saveTime(final long time) {
        return Observable.create(
                new Observable.OnSubscribe<Boolean>() {
                    @Override
                    public void call(Subscriber<? super Boolean> sub) {
                        boolean success = sharedPrefs.edit()
                                .putLong(Constants.SharedPreferencesConstants.KEY_LAUNCH_TIME, time)
                                .commit();
                        sub.onNext(success);
                        sub.onCompleted();
                    }
                }
        );
    }

    public Observable<Tweet> getTweetsWithTime() {
        String session = sharedPrefs.getString(Constants.SharedPreferencesConstants.KEY_SESSION, "");
        AppSession appSession = gson.fromJson(session, AppSession.class);
        customService = new TwitterTimeApiClient(appSession).getCustomService();

        final MostRetweetComparator comparator = new MostRetweetComparator();

        return Observable.interval(0, 60, TimeUnit.SECONDS)
                .flatMap(new Func1<Long, Observable<Search>>() {
                    @Override
                    public Observable<Search> call(Long aLong) {
                        return customService.tweets(getSearchString(), null, null, null, null, 5, null, null, null, false);
                    }
                }).flatMap(new Func1<Search, Observable<Tweet>>() {
                    @Override
                    public Observable<Tweet> call(Search search) {
                        Log.e("redo", getSearchString());
                        List<Tweet> modifiableList = new ArrayList<Tweet>(search.tweets);
                        Collections.sort(modifiableList, comparator);
                        return Observable.just(modifiableList.get(0));
                    }
                });
    }

    private String getSearchString() {
        Long launchTimeMillis = sharedPrefs.getLong(Constants.SharedPreferencesConstants.KEY_LAUNCH_TIME, 0);
        Date launchTime = launchTimeMillis.equals(0) ? new Date() : new Date(launchTimeMillis);

        String time12 = this.searchDateFormat12.format(launchTime);
        String time24 = this.searchDateFormat24.format(launchTime);

        return "\"It´s " + time12 + " and\""
                + " OR "
                + "\"It´s " + time24 + " and\""
                + " OR "
                + "\"It is " + time12 + " and\""
                + " OR "
                + "\"It is " + time24 + " and\"";
                //+ " OR "
                //+ "\"It's 22:50\"";
    }

    public class MostRetweetComparator implements Comparator<Tweet> {
        @Override
        public int compare(Tweet x, Tweet y) {
            int startComparison = compare(x.retweetCount, y.retweetCount);
            return startComparison != 0 ? startComparison
                    : compare(longFromCreatedAt(x.createdAt), longFromCreatedAt(y.createdAt));
        }

        // I don't know why this isn't in Long...
        private int compare(long a, long b) {
            return a < b ? -1
                    : a > b ? 1
                    : 0;
        }

        private long longFromCreatedAt(String createdAt) {
            try {
                Date date = twitterDateFormat.parse(createdAt);
                long time = date.getTime();
                return time;
            } catch (ParseException e) {
                e.printStackTrace();
                return 0;
            }
        }
    }
}
