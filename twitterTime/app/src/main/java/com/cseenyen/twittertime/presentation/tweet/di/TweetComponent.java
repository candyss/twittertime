package com.cseenyen.twittertime.presentation.tweet.di;

import com.cseenyen.twittertime.presentation.internal.di.ActivityScope;
import com.cseenyen.twittertime.presentation.internal.di.components.ApplicationComponent;
import com.cseenyen.twittertime.presentation.tweet.TweetActivity;

import dagger.Component;

/**
 * Created by cseenyen on 06/03/16.
 */

@ActivityScope
@Component(modules = TweetModule.class,
        dependencies = ApplicationComponent.class)
public interface TweetComponent {
    void inject(TweetActivity activity);

}
