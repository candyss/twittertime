package com.cseenyen.twittertime.presentation.tweet;

import com.cseenyen.twittertime.presentation.base.BaseLoadDataView;
import com.twitter.sdk.android.core.models.Tweet;

/**
 * Created by cseenyen on 06/03/16.
 */
public interface TweetLoadDataView extends BaseLoadDataView {
    /**
     * Display the tweet in the layout
     *
     * @param tweet
     *          Tweet to be displayed
     */
    void displayTweet(Tweet tweet);


    /**
     * Display message in case of error
     *
     * @param error
     *          The message to be shown
     */
    void displayError(int errorRes);
}
