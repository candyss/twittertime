package com.cseenyen.twittertime.data.network;

/**
 * Created by cseenyen on 25/02/16.
 */

import android.content.SharedPreferences;

import com.cseenyen.twittertime.data.Constants;
import com.google.gson.Gson;
import com.twitter.sdk.android.core.AppSession;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;

@Module
public final class ApiModule {

    public static final HttpUrl TWITTER_API_URL = HttpUrl.parse("https://api.twitter.com/1.1/search/tweets.json");

    static final int DISK_CACHE_SIZE = 50 * 1024 * 1024;

    @Provides
    @Singleton
    public TwitterTimeApiClient provideTwitterTimeApi(AppSession session) {
        return new TwitterTimeApiClient(session);
    }

    @Provides
    @Singleton
    public AppSession provideAppSession(SharedPreferences preferences, Gson gson) {
        String session = preferences.getString(Constants.SharedPreferencesConstants.KEY_SESSION, "");
        AppSession appSession = gson.fromJson(session, AppSession.class);
        return appSession;
    }

    @Provides
    @Singleton
    public TwitterCustomService provideCustomService(TwitterTimeApiClient client) {
        return client.getCustomService();
    }
}