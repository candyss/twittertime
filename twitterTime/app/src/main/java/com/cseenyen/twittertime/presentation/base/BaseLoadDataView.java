package com.cseenyen.twittertime.presentation.base;

import android.content.Context;

/**
 * Created by cseenyen on 27/02/16.
 */
public interface BaseLoadDataView {

    /**
     * Get a {@link android.content.Context}.
     */
    Context context();

    /**
     * Show a view with a progress bar indicating a loading process.
     */
    void showLoading();

    /**
     * Hide a loading view.
     */
    void hideLoading();
}
