package com.cseenyen.twittertime.presentation.tweet;

import android.support.annotation.NonNull;
import android.util.Log;

import com.cseenyen.twittertime.R;
import com.cseenyen.twittertime.domain.interactor.DefaultSubscriber;
import com.cseenyen.twittertime.domain.interactor.SaveSessionUseCase;
import com.cseenyen.twittertime.domain.interactor.SaveUpdatedTimeUseCase;
import com.cseenyen.twittertime.domain.interactor.SearchUseCase;
import com.cseenyen.twittertime.presentation.base.BasePresenter;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.AppSession;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;

import javax.inject.Inject;

import io.fabric.sdk.android.Fabric;

/**
 * Created by cseenyen on 06/03/16.
 */
public class TweetPresenter implements BasePresenter {
    private static final String TAG = TweetPresenter.class.getSimpleName();

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "P1g470LEhwlqaGt4XPxa3i4u2";
    private static final String TWITTER_SECRET = "mfBsYrUmf42fk1XAGmrVjfyJ6DCElL2kPD9mjmpzxaFPQxV88O";

    private TweetLoadDataView tweetView;
    private SearchUseCase searchUseCase;
    private SaveSessionUseCase saveSessionUseCase;
    private SaveUpdatedTimeUseCase saveUpdatedTimeUseCase;

    private AppSession twitterAppSession;

    @Inject
    public TweetPresenter(SearchUseCase searchUseCase, SaveSessionUseCase saveSessionUseCase, SaveUpdatedTimeUseCase useCase) {
        this.searchUseCase = searchUseCase;
        this.saveSessionUseCase = saveSessionUseCase;
        this.saveUpdatedTimeUseCase = useCase;
    }

    public void setView(@NonNull TweetLoadDataView view) {
        this.tweetView = view;
    }

    public void setup() {
        tweetView.showLoading();

        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(tweetView.context(), new Twitter(authConfig));
        getTwitterSession();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.tweetView = null;

        this.saveSessionUseCase.unsubscribe();
        this.searchUseCase.unsubscribe();
        this.saveUpdatedTimeUseCase.unsubscribe();
    }

    /**
     * Use Fabric library to create a guest session
     */
    public void getTwitterSession() {
        TwitterCore.getInstance().logInGuest(new Callback<AppSession>() {
            @Override
            public void success(Result<AppSession> result) {
                TweetPresenter.this.saveGuestSession(result.data);
            }

            @Override
            public void failure(TwitterException exception) {
                //Log.d(Statics.TAG, "loginGuest.callback.failure called");
                // unable to get an AppSession with guest auth
                throw exception;
            }
        });
    }

    public void saveGuestSession(AppSession session) {
        //this.twitterGuestSessionUseCase.execute(new SaveSessionSubscriber());
        this.saveSessionUseCase.setTwitterAppSession(session);
        this.saveSessionUseCase.execute(new SaveSessionSubscriber());
    }

    public void getTimeTweet() {
        this.searchUseCase.execute(new SearchTweetSubscriber());
    }

    public void displayError() {
        tweetView.hideLoading();
        tweetView.displayError(R.string.error_getting_tweet);
    }

    public void displayTweet(Tweet tweet) {
        tweetView.hideLoading();
        tweetView.displayTweet(tweet);
    }

    public void saveNewTime() {
        this.saveUpdatedTimeUseCase.execute(new SaveUpdatedimeSubscriber());
    }

    /**
     * Subscriber that will observe when the {@link AppSession} returned by {@link TwitterCore}
     * has been saved
     */
    private final class SaveSessionSubscriber extends DefaultSubscriber<Boolean> {

        @Override
        public void onCompleted() {
            Log.i(TAG, "SaveSessionSubscriber onCompleted");
            getTimeTweet();
        }

        @Override
        public void onError(Throwable e) {
            Log.w(TAG, "Error while saving session: " + e.getMessage());
        }

        @Override
        public void onNext(Boolean result) {
            Log.i(TAG, "SaveSessionSubscriber onNext with result: " + result);
        }
    }

    private final class SearchTweetSubscriber extends DefaultSubscriber<Tweet> {

        @Override
        public void onCompleted() {
            Log.i(TAG, "SearchTweetSubscriber onCompleted");
        }

        @Override
        public void onError(Throwable e) {
            Log.w(TAG, "Error while getting tweet: " + e.getMessage());
            displayError();
            saveNewTime();
        }

        @Override
        public void onNext(Tweet result) {
            Log.i(TAG, "SearchTweetSubscriber onNext with result: " + result);
            displayTweet(result);
            saveNewTime();
        }
    }

    /**
     * Subscriber that will observe when the launch time has been saved
     */
    private final class SaveUpdatedimeSubscriber extends DefaultSubscriber<Boolean> {

        @Override
        public void onCompleted() {
            Log.i(TAG, "SaveLaunchTimeSubscriber onCompleted");
        }

        @Override
        public void onError(Throwable e) {
            Log.w(TAG, "Error while saving new time: " + e.getMessage());
        }

        @Override
        public void onNext(Boolean result) {
            Log.i(TAG, "SaveLaunchTimeSubscriber onNext with result: " + result);
        }
    }
}
