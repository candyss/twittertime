package com.cseenyen.twittertime.data.network;

import com.twitter.sdk.android.core.AppSession;
import com.twitter.sdk.android.core.TwitterApiClient;

import javax.inject.Inject;

/**
 * Created by cseenyen on 06/03/16.
 */
public class TwitterTimeApiClient extends TwitterApiClient {

    @Inject
    public TwitterTimeApiClient(AppSession session) {
        super(session);
    }

    /**
     * Provide CustomService with defined endpoints
     */
    public TwitterCustomService getCustomService() {
        return getService(TwitterCustomService.class);
    }
}

