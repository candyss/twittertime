package com.cseenyen.twittertime.presentation.splash;

import com.cseenyen.twittertime.presentation.base.BaseLoadDataView;

/**
 * Created by cseenyen on 12/02/16.
 */
public interface SplashLoadDataView extends BaseLoadDataView {

    /**
     * Navigates to the next view
     */
    public void navigateToNext();
}