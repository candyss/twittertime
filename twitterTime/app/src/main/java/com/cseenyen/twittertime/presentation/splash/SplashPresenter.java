package com.cseenyen.twittertime.presentation.splash;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.cseenyen.twittertime.domain.interactor.DefaultSubscriber;
import com.cseenyen.twittertime.domain.interactor.SaveLaunchTimeUseCase;
import com.cseenyen.twittertime.presentation.base.BasePresenter;
import com.cseenyen.twittertime.presentation.internal.di.ActivityScope;

import javax.inject.Inject;

/**
 * Created by cseenyen on 12/02/16.
 */

@ActivityScope
public class SplashPresenter implements BasePresenter {

    private SplashLoadDataView splashView;

    @Inject
    public SharedPreferences sharedPrefs;

    private SaveLaunchTimeUseCase saveLaunchTimeUseCase;

    @Inject
    public SplashPresenter(SaveLaunchTimeUseCase useCase) {
        this.saveLaunchTimeUseCase = useCase;
    }

    public void setView(@NonNull SplashLoadDataView view) {
        splashView = view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        splashView = null;
        this.saveLaunchTimeUseCase.unsubscribe();
    }

    public void saveLaunchTime() {
        this.saveLaunchTimeUseCase.execute(new SaveLaunchTimeSubscriber());
    }

    /**
     * Subscriber that will observe when the launch time has been saved
     */
    private final class SaveLaunchTimeSubscriber extends DefaultSubscriber<Boolean> {

        @Override
        public void onCompleted() {
            splashView.navigateToNext();
        }

        @Override
        public void onError(Throwable e) {
        }

        @Override
        public void onNext(Boolean result) {
        }
    }

}
