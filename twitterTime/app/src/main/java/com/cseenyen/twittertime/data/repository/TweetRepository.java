package com.cseenyen.twittertime.data.repository;

import com.twitter.sdk.android.core.AppSession;
import com.twitter.sdk.android.core.models.Tweet;

import rx.Observable;

/**
 * Created by cseenyen on 27/02/16.
 */
public interface TweetRepository {
    /**
     * Get an {@link rx.Observable} which will emit a {@link Boolean} indicating if the time has been
     * saved successfully.
     */
    Observable<Boolean> saveLaunchTime();

    /**
     * Get an {@link rx.Observable} which will emit a {@link Boolean} indicating if the session has been
     * saved successfully.
     */
    Observable<Boolean> saveSession(AppSession twitterAppSession);

    /**
     * Get an {@link rx.Observable} which will emit a {@link Tweet} containing the time text.
     */
    Observable<Tweet> timeTweet();

    /**
     * Get an {@link rx.Observable} which will emit a {@link Boolean} indicating if the time has been
     * updated successfully.
     */
    Observable<Boolean> saveUpdatedTime();
}
