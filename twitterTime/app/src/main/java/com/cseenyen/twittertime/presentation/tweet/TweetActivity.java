package com.cseenyen.twittertime.presentation.tweet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cseenyen.twittertime.R;
import com.cseenyen.twittertime.presentation.base.BaseActivity;
import com.cseenyen.twittertime.presentation.internal.di.components.ApplicationComponent;
import com.cseenyen.twittertime.presentation.tweet.di.DaggerTweetComponent;
import com.cseenyen.twittertime.presentation.tweet.di.TweetModule;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.TweetView;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by cseenyen on 06/03/16.
 */
public class TweetActivity extends BaseActivity implements TweetLoadDataView {

    @Inject
    TweetPresenter tweetPresenter;

    @Bind(R.id.tweet_layout)
    LinearLayout tweetLayout;
    @Bind(R.id.tweet_error)
    TextView tweetErrorText;

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, TweetActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        tweetPresenter.setView(this);
        tweetPresenter.setup();
    }

    @Override
    protected void setupActivityComponent(ApplicationComponent appComponent) {
        DaggerTweetComponent.builder()
                .applicationComponent(getApplicationComponent())
                .tweetModule(new TweetModule())
                .build()
                .inject(this);
    }

    @Override
    public Context context() {
        return getApplicationContext();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void displayTweet(Tweet tweet) {
        this.tweetErrorText.setVisibility(View.GONE);
        this.tweetLayout.setVisibility(View.VISIBLE);

        this.tweetLayout.removeAllViews();
        this.tweetLayout.addView(new TweetView(TweetActivity.this, tweet));
    }

    @Override
    public void displayError(int errorRes) {
        this.tweetLayout.setVisibility(View.GONE);
        this.tweetErrorText.setVisibility(View.VISIBLE);

        this.tweetErrorText.setText(getString(errorRes));
    }
}
