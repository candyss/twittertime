package com.cseenyen.twittertime.presentation.splash.di;

import com.cseenyen.twittertime.presentation.internal.di.ActivityScope;
import com.cseenyen.twittertime.presentation.internal.di.components.ApplicationComponent;
import com.cseenyen.twittertime.presentation.splash.SplashActivity;

import dagger.Component;

/**
 * Created by cseenyen on 27/02/16.
 */

@ActivityScope
@Component(dependencies = ApplicationComponent.class,
        modules = SplashModule.class)
public interface SplashComponent {

    void inject(SplashActivity splashActivity);
}
