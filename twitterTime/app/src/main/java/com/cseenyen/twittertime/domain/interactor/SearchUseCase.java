package com.cseenyen.twittertime.domain.interactor;

import com.cseenyen.twittertime.data.repository.TweetRepository;
import com.cseenyen.twittertime.domain.executor.PostExecutionThread;
import com.cseenyen.twittertime.domain.executor.ThreadExecutor;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by cseenyen on 06/03/16.
 */
public class SearchUseCase extends UseCase {

    public TweetRepository tweetRepository;

    @Inject
    public SearchUseCase(TweetRepository tweetRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.tweetRepository = tweetRepository;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.tweetRepository.timeTweet();
    }
}
