package com.cseenyen.twittertime.presentation.splash;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.cseenyen.twittertime.R;
import com.cseenyen.twittertime.presentation.base.BaseActivity;
import com.cseenyen.twittertime.presentation.internal.di.components.ApplicationComponent;
import com.cseenyen.twittertime.presentation.splash.di.DaggerSplashComponent;
import com.cseenyen.twittertime.presentation.splash.di.SplashComponent;
import com.cseenyen.twittertime.presentation.splash.di.SplashModule;
import com.cseenyen.twittertime.presentation.tweet.TweetActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

/**
 * Created by cseenyen on 12/02/16.
 */
public class SplashActivity extends BaseActivity implements SplashLoadDataView {

    SplashComponent splashComponent;

    @Inject
    SplashPresenter splashPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);
        ButterKnife.bind(this);

        splashPresenter.setView(this);
        splashPresenter.saveLaunchTime();
    }

    @Override
    protected void setupActivityComponent(ApplicationComponent appComponent) {
        splashComponent = DaggerSplashComponent.builder()
                .applicationComponent(getApplicationComponent())
                .splashModule(new SplashModule())
                .build();
        splashComponent.inject(this);
    }

    @Override
    public Context context() {
        return getApplicationContext();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void navigateToNext() {
        Intent intentToLaunch = TweetActivity.getCallingIntent(this);
        startActivity(intentToLaunch);
        finish();
    }
}
