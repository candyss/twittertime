package com.cseenyen.twittertime.domain.interactor;

import com.cseenyen.twittertime.data.repository.TweetRepository;
import com.cseenyen.twittertime.domain.executor.PostExecutionThread;
import com.cseenyen.twittertime.domain.executor.ThreadExecutor;
import com.twitter.sdk.android.core.AppSession;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by cseenyen on 06/03/16.
 */
public class SaveSessionUseCase extends UseCase {

    private TweetRepository tweetRepository;
    private AppSession twitterAppSession;

    @Inject
    public SaveSessionUseCase(TweetRepository tweetRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.tweetRepository = tweetRepository;
    }

    public void setTwitterAppSession(AppSession twitterAppSession) {
        this.twitterAppSession = twitterAppSession;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        if(this.twitterAppSession == null) {
            throw new NullPointerException("TwitterAppSession is null");
        }

        return this.tweetRepository.saveSession(this.twitterAppSession);
    }
}
