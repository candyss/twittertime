package com.cseenyen.twittertime.presentation.tweet;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.cseenyen.twittertime.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by cseenyen on 07/03/16.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class TweetActivityTest {

    /**
     * {@link ActivityTestRule} is a JUnit {@link Rule @Rule} to launch your activity under test.
     * <p/>
     * <p/>
     * Rules are interceptors which are executed for each test method and are important building
     * blocks of Junit tests.
     */
    @Rule
    public ActivityTestRule<TweetActivity> tweetActivity = new ActivityTestRule<>(TweetActivity.class);

    @Test
    public void launchMainView() {
        onView(withId(R.id.tweet_layout)).check(matches(isDisplayed()));
        onView(withId(R.id.tweet_error)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    @Test
    public void onErrorDisplayErrorMessage() {
        final int error = R.string.error_getting_tweet;
        getInstrumentation().runOnMainSync(new Runnable() {
            public void run() {
                tweetActivity.getActivity().displayError(error);
            }
        });

        onView(withId(R.id.tweet_layout)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
        onView(withId(R.id.tweet_error)).check(matches(isDisplayed()));
        onView(withId(R.id.tweet_error)).check(matches(withText("Oops! We haven\'t found any tweet with the current time! Please try again later!")));
    }
}
